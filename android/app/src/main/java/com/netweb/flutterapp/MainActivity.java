package com.netweb.flutterapp;

import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "flutter.native/helper";
  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
    GeneratedPluginRegistrant.registerWith(flutterEngine);
    new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(),CHANNEL)
            .setMethodCallHandler((call, result) -> {
              if (call.method.equals("helloFromNativeCode")) {
                String greetings = helloFromNativeCode();
                result.success(greetings);
              }
            });

  }


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
    new AlertDialog.Builder(MainActivity.this).setMessage("hi").show();
  }

  private String helloFromNativeCode() {
      return "Hello from Native Android Code";
    }



}
